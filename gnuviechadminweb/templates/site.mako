# -*- coding: utf-8 -*-
<html>
<head>
${self.headlines()}
${h.javascript_include_tag('jquery.js')}
${h.stylesheet_link_tag('gva.css')}
</head>
<body>
<ul id="menu">
% for item in c.menu:
 <li class="${h.cssclasses(item)}">${h.menulink(item)}</li>
% endfor
</ul>
<div id="content">
% if c.messages['errors']:
<ul id="errors" class="msgbox">
% for msg in c.messages['errors']:
  <li>${msg}</li> 
% endfor
</ul>
% endif
% if c.messages['messages']:
<ul id="messages" class="msgbox">
% for msg in c.messages['messages']:
  <li>${msg}</li>
% endfor
</ul>
% endif
${next.body()}
</div>
</body>
</html>
<%def name="headlines()">
  <title>Site</title>
</%def>
