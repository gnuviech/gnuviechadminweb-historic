# -*- coding: utf-8 -*-
<%inherit file="site.mako" />
<h1>Login</h1>
${h.form(h.url(action='login'), method='post')}
User name: ${h.text_field('username')}
Password:  ${h.password_field('password')}
${h.submit('Login')}
${h.end_form()}
<%def name="headlines()">
  <title>Login</title>
</%def>