# -*- coding: utf-8 -*-
<%inherit file="site.mako" />
<h1>Change your password</h1>
${h.form(h.url(action='updatepassword'), method='post')}
<table>
 <tr>
  <td><label for="oldpassword">Old password:</label></td>
  <td>${h.password_field('oldpassword')}</td>
 </tr>
 <tr>
  <td><label for="password">New password:</label></td>
  <td>${h.password_field('password')}</td>
 </tr>
 <tr>
  <td><label for="confirm">Confirm new password:</label></td>
  <td>${h.password_field('confirm')}</td>
 </tr>
 <tr>
  <td colspan="2">${h.submit('Change password')}</td>
 </tr>
</table>
${h.end_form()}
<%def name="headlines()">
  <title>Change your password</title>
</%def>
