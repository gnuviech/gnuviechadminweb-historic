"""Helper functions

Consists of functions to typically be used within templates, but also
available to Controllers. This module is available to both as 'h'.
"""
from webhelpers import *

def cssclasses(menuitem):
    cssclassnames=['menuitem']
    return " ".join(cssclassnames)

def menulink(menuitem):
    return link_to(menuitem.title, url(controller=menuitem.controller,
                                       action=menuitem.action))
