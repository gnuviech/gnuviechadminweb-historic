"""The base Controller API

Provides the BaseController class for subclassing, and other objects
utilized by Controllers.
"""
from pylons import c, cache, config, g, request, response, session
from pylons.controllers import WSGIController
from pylons.controllers.util import abort, etag_cache, redirect_to
from pylons.decorators import jsonify, validate
from pylons.i18n import _, ungettext, N_
from pylons.templating import render

import gnuviechadminweb.lib.helpers as h
import gnuviechadminweb.model as model
from gnuviechadminweb.model import meta

class BaseController(WSGIController):

    def __call__(self, environ, start_response):
        """Invoke the Controller"""
        conn = meta.engine.connect()
        meta.Session.configure(bind=conn)
        c.menu = model.Menu.allowed(environ['authkit.users'].user_roles(
                environ['REMOTE_USER']) if 'REMOTE_USER' in environ else [])
        c.messages = {'errors' : [],
                      'messages' : []}
        try:
            # WSGIController.__call__ dispatches to the Controller method
            # the request is routed to. This routing information is
            # available in environ['pylons.routes_dict']
            return WSGIController.__call__(self, environ, start_response)
        finally:
            meta.Session.remove()
            conn.close()

# Include the '_' function in the public names
__all__ = [__name for __name in locals().keys() if not __name.startswith('_') \
           or __name == '_']
