# -*- coding: utf-8 -*-
import logging

from gnuviechadminweb.lib.base import *
from authkit.permissions import ValidAuthKitUser
from authkit.authorize.pylons_adaptors import authorize

log = logging.getLogger(__name__)

class GvaController(BaseController):
    def index(self):
        # Return a rendered template
        return render('/main.mako')

    @authorize(ValidAuthKitUser())
    def password(self):
        return render('/chpassword.mako')

    @authorize(ValidAuthKitUser())
    def updatepassword(self):
        users = request.environ['authkit.users']
        if users.user_has_password(request.environ['REMOTE_USER'],
                                   request.params['oldpassword']):
            if request.params['password'] != request.params['confirm']:
                c.messages['errors'].append("New password and confirmation don't match")
            elif len(request.params['password']) < 8:
                c.messages['errors'].append("Your new password is too short. It must consist of at least 8 characters")
            else:
                users.user_set_password(request.environ['REMOTE_USER'],
                                        request.params['password'])
                c.messages['messages'].append("Your password has been changed.")
        else:
            c.messages['errors'].append("Your old password is not correct.")
        if c.messages['errors']:
            return render('/chpassword.mako')
        return render('/main.mako')
