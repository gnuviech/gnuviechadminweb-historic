import logging

from gnuviechadminweb.lib.base import *
from authkit.permissions import HasAuthKitRole
from authkit.authorize.pylons_adaptors import authorize

log = logging.getLogger(__name__)

class MenuController(BaseController):
    @authorize(HasAuthKitRole(["menuedit"]))
    def index(self):
        # Return a rendered template
        return render('/menuindex.mako')
