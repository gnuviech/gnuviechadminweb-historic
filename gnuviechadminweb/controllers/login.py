import logging

from gnuviechadminweb.lib.base import *
from authkit.permissions import ValidAuthKitUser
from authkit.authorize.pylons_adaptors import authorize

log = logging.getLogger(__name__)

class LoginController(BaseController):

    def index(self):
        # Return a rendered template
        return render('/loginform.mako')

    @authorize(ValidAuthKitUser())
    def login(self):
        c.messages['messages'].append('Successfully logged in.')
        return render('/main.mako')

    def logout(self):
        return redirect_to(action = 'loggedout')

    def loggedout(self):
        c.messages['messages'].append('You are logged out.')
        return render('/main.mako')
