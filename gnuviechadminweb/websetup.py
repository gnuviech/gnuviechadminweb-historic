"""Setup the gnuviechadminweb application"""
import logging

from paste.deploy import appconfig
from pylons import config

from gnuviechadminweb.config.environment import load_environment
from gnuviechadminweb.model import meta, User, Group, Role
from gnuviechadminweb.model.GVAUsers import GVAUsers, sha512

log = logging.getLogger(__name__)

def setup_config(command, filename, section, vars):
    """Place any commands to setup gnuviechadminweb here"""
    conf = appconfig('config:' + filename)
    load_environment(conf.global_conf, conf.local_conf)

    log.info("Creating tables")
    meta.metadata.create_all(bind=meta.engine)

    log.info("Creating default admin user, role and group")
    users = GVAUsers('gnuviechadminweb.model:meta', sha512)
    users.role_create("admin")
    users.group_create("administrators")

    users.user_create("admin", "admin", "administrators")
    users.user_add_role("admin", "admin")

    log.info("Successfully setup")
