# -*- python -*-
# -*- coding: utf-8 -*-
from sqlalchemy import orm

from gnuviechadminweb.model.tables import *
import logging

log = logging.getLogger(__name__)

class Group(object):
  pass

class Role(object):
  pass

class User(object):
  pass

orm.mapper(Group, t_group, {
    'users' : orm.relation(User),
    })

orm.mapper(Role, t_role, properties = {
    'users' : orm.relation(User, secondary = t_user_role),
    })

orm.mapper(User, t_user, properties = {
    'roles' : orm.relation(Role, secondary = t_user_role),
    'group' : orm.relation(Group)
    })
