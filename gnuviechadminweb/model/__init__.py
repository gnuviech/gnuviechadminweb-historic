import sqlalchemy as sa
from sqlalchemy import orm

from gnuviechadminweb.model import meta

def init_model(engine):
    """Call me before using any of the tables or classes in the model."""
    
    sm = orm.sessionmaker(autoflush=True, transactional=True, bind=engine)
    
    meta.engine = engine
    meta.Session = orm.scoped_session(sm)

from gnuviechadminweb.model.user import Group, Role, User
from gnuviechadminweb.model.GVAUsers import GVAUsers, sha512
from gnuviechadminweb.model.menu import Menu
