# -*- python -*-
# -*- coding: utf-8 -*-
import sqlalchemy as sa

from gnuviechadminweb.model import meta

t_group = \
    sa.Table("group", meta.metadata,
             sa.Column("id", sa.types.Integer, primary_key=True),
             sa.Column("name", sa.types.String(40), nullable=False,
                       unique=True),
             )

t_role = \
    sa.Table("role", meta.metadata,
             sa.Column("id", sa.types.Integer, primary_key=True),
             sa.Column("name", sa.types.String(40), nullable=False)
             )

t_user = \
    sa.Table("user", meta.metadata,
             sa.Column("id", sa.types.Integer, primary_key=True),
             sa.Column("name", sa.types.String(40), nullable=False),
             sa.Column("password", sa.types.String(128), nullable=False),
             sa.Column("group_id", sa.types.Integer,
                       sa.ForeignKey(t_group.c.id))
             )

t_user_role = \
    sa.Table("user_role", meta.metadata,
             sa.Column("id", sa.types.Integer, primary_key=True),
             sa.Column("user_id", sa.types.Integer, sa.ForeignKey(t_user.c.id)),
             sa.Column("role_id", sa.types.Integer, sa.ForeignKey(t_role.c.id))
             )         
