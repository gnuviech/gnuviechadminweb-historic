# -*- coding: utf-8 -*-

class Menu(object):
    menuitems = [
        ('Main page', 'gva', 'index', None),
        ('Login', 'login', 'login', []),
        ('Logout', 'login', 'logout', ['*']),
        ('Manage clients', 'clients', 'index', ['admin']),
        ('Update Profile', 'client', 'changes', ['client']),
        ('System accounts', 'sysuser', 'index', ['admin']),
        ('System account', 'sysuser', 'account', ['client']),
        ('Mail accounts', 'mailaccount', 'index', ['admin', 'sysuser']),
        ('Change password', 'gva', 'password', ['*'])
        ]

    @classmethod
    def allowed(cls, roles=[]):
        items = []
        for item in cls.menuitems:
            additem = False
            if item[3] is None:
                additem = True
            elif len(item[3]) == 0 and len(roles) == 0:
                additem = True
            elif len(roles) > 0:
                for role in item[3]:
                    if role in roles or role == '*':
                        additem = True
                        break
            if additem:
                items.append(Menu(item[0], item[1], item[2]))
        return items

    def __init__(self, title, controller, action):
        self.title = title
        self.controller = controller
        self.action = action
