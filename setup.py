try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='gnuviechadminweb',
    version="0.1",
    description='web interface for gnuviechadmin',
    author='Jan Dittberner',
    author_email='jan@dittberner.info',
    url='http://www.gnuviech-server.de/projects/gnuviechadminweb',
    install_requires=["Pylons>=0.9.6.2", "SQLAlchemy>=0.4", "AuthKit>=0.4"],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    test_suite='nose.collector',
    package_data={'gnuviechadminweb': ['i18n/*/LC_MESSAGES/*.mo']},
    message_extractors = {'gnuviechadminweb': [
            ('**.py', 'python', None),
            ('templates/**.mako', 'mako', None),
            ('public/**', 'ignore', None)]},
    entry_points="""
    [paste.app_factory]
    main = gnuviechadminweb.config.middleware:make_app

    [paste.app_install]
    main = pylons.util:PylonsInstaller
    """,
)
